import os
import os.path
import shutil
import wave
import struct
import progressbar
from pydub import AudioSegment
from pydub.effects import normalize
from subprocess import Popen

RES = (1920, 1080)
CODEC = 'libx264' # 'libx264', 'theora' or 'libvpx'
FPS = 60
OFPS = 25
OUTPUT = 'output/video' # No extension
CONFIG = ''

if CODEC == 'theora': OUTPUT += '.ogv'
elif CODEC == 'libx264': OUTPUT += '.mp4'
else: OUTPUT += '.webm'

if CODEC == 'theora': CONFIG += '-q:v 4 -q:a 5'
elif CODEC == 'libx264': CONFIG += '-strict -2'
else: CONFIG += '-b:v 200K -crf 10'

scenes = [v.split(' ') for v in open('scenes_for_robots.txt').read().split('\n') if len(v)]

try: shutil.rmtree('frames')
except: pass
os.mkdir('frames')

audio_bits = []

next_frame = 0

bar = progressbar.ProgressBar(max_value=len(scenes))
print 'Generating frames...'

processes = []
frames = []
for scene in bar(scenes):
    fname = os.path.splitext(scene[0])[0] + '.png'
    p = Popen(['convert', scene[0], '-resize', '%dx%d' % (RES[0], RES[1]), '-alpha', 'off', fname])
    processes.append(p)
    
    bit = AudioSegment.from_wav(scene[1])
    audio_bits.append(bit)
    
    frames.append((int(bit.duration_seconds*FPS), fname, next_frame))
    next_frame += int(bit.duration_seconds * FPS)
    
    if len(processes) >= 9:
        processes[0].wait()
        processes.pop(0)

for p in processes: p.wait()

bar = progressbar.ProgressBar(max_value=len(frames))
print 'Copying frames...'

for f in bar(frames):
    for i in range(f[2], f[2]+f[0]):
        os.system('ln %s frames/frame%05d.png' % (f[1], i))
    os.remove(f[1])
    
normalize(reduce(lambda x,y:x+y, audio_bits)).export('audio.wav', format='wav')

os.system('ffmpeg -framerate %d -i frames/frame%s05d.png -i audio.wav -vcodec %s -c:a aac %s -threads 0 -y -pix_fmt yuv420p -shortest -r %d %s' % (FPS, '%', CODEC, CONFIG, OFPS, OUTPUT))

os.remove('audio.wav')
shutil.rmtree('frames')
